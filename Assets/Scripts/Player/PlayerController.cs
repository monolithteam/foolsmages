﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviourPunCallbacks
{
    public float moveSpeed = 10f;

    MultipleTargetCamera camera;
    Rigidbody rb;
    FixedJoystick joystick;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        joystick = GameObject.FindWithTag("Joystick").GetComponent<FixedJoystick>();
        camera = FindObjectOfType<MultipleTargetCamera>();

        camera.AddNewPlayer(gameObject);
    }

    void FixedUpdate()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            return;
        }

        rb.velocity = new Vector3(joystick.Horizontal * moveSpeed, rb.velocity.y, joystick.Vertical * moveSpeed);

        if (joystick.Horizontal != 0f || joystick.Vertical != 0f)
        {
            //Character rotation
            transform.rotation = Quaternion.LookRotation(rb.velocity);
            //TODO: Walk animation true
        }
        else
        {
            //TODO: Walk animation false
        }
    }
}
