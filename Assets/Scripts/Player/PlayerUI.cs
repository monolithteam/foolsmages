﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [SerializeField]
    private Vector3 screenOffset = new Vector3(0f, 30f, 0f);

    float characterHeight = 0f;
    Transform targetTransform;
    Renderer targetRenderer;
    CanvasGroup _canvasGroup;
    Vector3 targetPosition;

    [SerializeField]
    private Text playerNameText;
    [SerializeField]
    private Slider playerHealthSlider;

    PlayerManager target;

    void Awake()
    {
        this.transform.SetParent(GameObject.Find("UICanvas").GetComponent<Transform>(), false);
        _canvasGroup = this.GetComponent<CanvasGroup>();
    }

    void Update()
    {
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }

        if (playerHealthSlider != null)
        {
            playerHealthSlider.value = target.currentHealth;
        }
    }

    void LateUpdate()
    {
        if (targetRenderer != null)
        {
            this._canvasGroup.alpha = targetRenderer.isVisible ? 1f : 0f;
        }

        if (targetTransform != null)
        {
            targetPosition = targetTransform.position;
            targetPosition.y += characterHeight;
            this.transform.position = Camera.main.WorldToScreenPoint(targetPosition) + screenOffset;
        }
    }

    public void SetTarget(PlayerManager _target)
    {
        if (_target == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> PlayerManager target for PlayerUI.SetTarget.", this);
            return;
        }

        target = _target;
        if (playerNameText != null)
        {
            playerNameText.text = target.photonView.Owner.NickName;
        }

        targetTransform = this.target.GetComponent<Transform>();
        targetRenderer = this.target.GetComponent<Renderer>();
        characterHeight = 1f;
    }

    /*public void DecreaseHealth(int value)
    {
        target.currentHealth -= value;
        StartCoroutine(LerpHealthBar());
    }

    public void IncreaseHealth(int value)
    {
        target.currentHealth += value;
        StartCoroutine(LerpHealthBar());
    }

    IEnumerator LerpHealthBar()
    {
        float time = 0f;
        float waitTime = 1f;

        while (time < waitTime)
        {
            playerHealthSlider.value = Mathf.Lerp(playerHealthSlider.value, target.currentHealth, (time / waitTime));
            time += Time.deltaTime;

            yield return null;
        }

        playerHealthSlider.value = target.currentHealth;
        yield return null;
    }*/
}
