﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsController : MonoBehaviourPunCallbacks, IPunObservable
{
    [SerializeField]
    private Transform elementsPanelParent;
    [SerializeField]
    private GameObject buttonPrefab;

    ElementsModel elementsModel;
    ElementsSpheresModel elementsSpheres;

    void Start()
    {
        elementsModel = GetComponent<ElementsModel>();

        StartCoroutine(StartCor());

        ShuffleElementsQueue();

        StartCoroutine(WaitBeforeAddElement(0));
    }

    IEnumerator StartCor()
    {
        yield return new WaitForSeconds(1.5f);

        elementsSpheres = GameObject.Find(PhotonNetwork.LocalPlayer.NickName.ToString()).GetComponentInChildren<ElementsSpheresModel>();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(elementsSpheres.spheres[0]);
            stream.SendNext(elementsSpheres.spheres[1]);
        }
        else
        {
            this.elementsSpheres.spheres[0] = (GameObject)stream.ReceiveNext();
            this.elementsSpheres.spheres[1] = (GameObject)stream.ReceiveNext();
        }
    }

    public void OnAddElement(Element clickedElement)
    {
        elementsModel.elementsCombination[1] = elementsModel.elementsCombination[0];
        elementsModel.elementsCombination[0] = clickedElement;

        for (int i = 0; i < elementsModel.elementsCombination.Length; i++)
        {
            if (elementsModel.elementsCombination[i] == Element.None)
            {
                return;
            }
            else
            {
                elementsSpheres.CallRPC(i, elementsModel.elementsCombination[i]);
            }
        }
    }

    void ShuffleElementsQueue()
    {
        for (int t = 0; t < elementsModel.elementsQueue.Length; t++)
        {
            Element tmp = elementsModel.elementsQueue[t];
            int r = Random.Range(t, elementsModel.elementsQueue.Length);
            elementsModel.elementsQueue[t] = elementsModel.elementsQueue[r];
            elementsModel.elementsQueue[r] = tmp;
        }
    }

    void AddRandomElement()
    {
        ElementButtonController randomElement = Instantiate(buttonPrefab, elementsPanelParent).GetComponent<ElementButtonController>();
        randomElement.element = elementsModel.elementsQueue[elementsModel.currentElements];
        randomElement.SetButtonText();

        elementsModel.currentElements += 1;
    }

    IEnumerator WaitBeforeAddElement(int seconds)
    {
        yield return new WaitForSeconds(seconds);

        AddRandomElement();

        if (elementsModel.currentElements != elementsModel.maxElements)
        {
            StartCoroutine(WaitBeforeAddElement(elementsModel.timeBeforeAddElement));
        }
    }
}
