﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpellDatabaseController : EventCommand
{
    [Inject]
    public SpellModel model { get; set; }

    public override void Execute()
    {
        AddSpellsToDatabase();

        foreach(KeyValuePair<Tuple<Element, Element>, GameObject> spell in model.spells)
        {
            Debug.Log("Key = " + spell.Key + " Value = " + spell.Value);
        }
    }

    void AddSpellsToDatabase()
    {
        model.spells = new Dictionary<Tuple<Element, Element>, GameObject>();

        model.spells.Add(new Tuple<Element, Element>(Element.Fire, Element.Fire), Resources.Load<GameObject>("Spells/Fire_Fire"));
        model.spells.Add(new Tuple<Element, Element>(Element.Water, Element.Water), Resources.Load<GameObject>("Spells/Water_Water"));
        model.spells.Add(new Tuple<Element, Element>(Element.Earth, Element.Earth), Resources.Load<GameObject>("Spells/Earth_Earth"));
        model.spells.Add(new Tuple<Element, Element>(Element.Air, Element.Air), Resources.Load<GameObject>("Spells/Air_Air"));
        model.spells.Add(new Tuple<Element, Element>(Element.Dark, Element.Dark), Resources.Load<GameObject>("Spells/Dark_Dark"));
        model.spells.Add(new Tuple<Element, Element>(Element.Light, Element.Light), Resources.Load<GameObject>("Spells/Light_Light"));
    }
}
