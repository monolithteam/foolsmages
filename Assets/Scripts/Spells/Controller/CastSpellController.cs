﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.command.impl;
using System;
using Photon.Pun;

public class CastSpellController : EventCommand
{
    [Inject]
    public ElementsModel model { get; set; }

    [Inject]
    public SpellModel spellModel { get; set; }

    public override void Execute()
    {
        Debug.Log("<color=#FF5733>Cast Spell Entered</color>");
        GameObject firePoint = evt.data as GameObject;

        SpawnSpellVFX(firePoint);
    }
    
    void SpawnSpellVFX(GameObject firePoint)
    {
        GameObject vfx;

        GameObject spellToSpawn = FindSpell();

        if(firePoint != null && spellToSpawn != null)
        {
            vfx = PhotonNetwork.Instantiate("Spells/" + spellToSpawn.name, firePoint.transform.position, Quaternion.identity);

            vfx.transform.localRotation = firePoint.transform.rotation;
        }
        else if(firePoint == null)
        {
            Debug.Log("<color=#FF5733>No Fire Point</color>");
        }
        else if(spellToSpawn == null)
        {
            Debug.Log("<color=#FF5733>No Spell</color>");
        }
        else
        {
            Debug.Log("<color=#FF5733>Error cast spell</color>");
        }
    }

    GameObject FindSpell()
    {
        GameObject spell = null;

        Tuple<Element, Element> key = new Tuple<Element, Element>(model.elementsCombination[0], model.elementsCombination[1]);
        Tuple<Element, Element> keyReversed = new Tuple<Element, Element>(model.elementsCombination[1], model.elementsCombination[0]);

        if(spellModel.spells.TryGetValue(key, out spell))
        {
            Debug.Log("<color=#33FF6E>Spell found: " + spell.gameObject.name + "</color>");
            spellModel.spells.Remove(key);
            spellModel.spells.Remove(keyReversed);
            return spell;
        }
        else
        {
            return null;
        }
    }
}
