﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElementButtonController : MonoBehaviour
{
    public Element element;

    Button button;
    Text buttonText;

    ElementsController elementController;
    ElementsModel elementsModel;

    void Awake()
    {
        elementController = FindObjectOfType<ElementsController>();
        elementsModel = FindObjectOfType<ElementsModel>();
        button = GetComponent<Button>();
        buttonText = GetComponentInChildren<Text>();

        button.onClick.AddListener(OnClickElement);
    }

    public void SetButtonText()
    {
        buttonText.text = element.ToString();
    }

    void OnClickElement()
    {
        elementController.OnAddElement(element);
    }
}
