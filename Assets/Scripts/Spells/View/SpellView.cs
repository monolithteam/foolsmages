﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.dispatcher.eventdispatcher.api;
using UnityEngine.UI;

public class SpellView : BaseView
{
    [Header("Base gameObj")]
    [SerializeField]
    private GameObject firePoint;

    [Header("CastButton")]
    private Button castSpellButton;

    internal void Init()
    {
        castSpellButton = GameObject.FindGameObjectWithTag("CastSpell").GetComponent<Button>();

        castSpellButton.onClick.AddListener(CastSpell);

        dispatcher.Dispatch(MainEvents.SpellDatabase);
    }

    void CastSpell()
    {
        if(photonView.IsMine)
        {
            dispatcher.Dispatch(MainEvents.CastSpell, firePoint);
        }
    }
}
