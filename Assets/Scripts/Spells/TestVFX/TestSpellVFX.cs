﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestSpellVFX : MonoBehaviour
{
    [Header("Buttons")]
    [SerializeField]
    private Button loadSpellButton;
    [SerializeField]
    private Button previousSpellButton;
    [SerializeField]
    private Button nextSpellButton;
    [SerializeField]
    private Button stopSpellButton;

    [Header("Text")]
    [SerializeField]
    private Text currentSpellText;

    [Header("Components")]
    [SerializeField]
    private GameObject[] spellPrefab;
    
    private int currentSpellId;
    private GameObject currentSpellPlayed;

    void Start()
    {
        currentSpellId = 0;

        loadSpellButton.onClick.AddListener(SpawnSpell);
        previousSpellButton.onClick.AddListener(PreviousSpell);
        nextSpellButton.onClick.AddListener(NextSpell);
        stopSpellButton.onClick.AddListener(StopSpell);

        UpdateSpellTextName();
    }

    void SpawnSpell()
    {
        StopSpell();

        currentSpellPlayed = Instantiate(spellPrefab[currentSpellId], spellPrefab[currentSpellId].transform.position, spellPrefab[currentSpellId].transform.rotation);
    }

    void PreviousSpell()
    {
        if(currentSpellId > 0)
        {
            currentSpellId -= 1;
        }
        else
        {
            currentSpellId = spellPrefab.Length - 1;
        }

        UpdateSpellTextName();
    }

    void NextSpell()
    {
        if(currentSpellId < spellPrefab.Length - 1)
        {
            currentSpellId += 1;
        }
        else
        {
            currentSpellId = 0;
        }
        
        UpdateSpellTextName();
    }

    void UpdateSpellTextName()
    {
        currentSpellText.text = spellPrefab[currentSpellId].name;
    }

    void StopSpell()
    {
        if(currentSpellPlayed != null)
        {
            Destroy(currentSpellPlayed);
        }
    }
}
