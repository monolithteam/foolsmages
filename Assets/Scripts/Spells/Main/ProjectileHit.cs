﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHit : MonoBehaviour
{
    [SerializeField]
    private float damage;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerManager>().DecreaseHealth(50);
            Destroy(gameObject.GetComponent<SphereCollider>());
        }
    }
}
