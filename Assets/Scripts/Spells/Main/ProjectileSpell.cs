﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileSpell : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float timeToDestroy;

    public GameObject muzzlePrefab;
    public GameObject hitPrefab;

    void Start()
    {
        photonView.RPC("CastSpell", RpcTarget.Others);
    }

    void Update()
    {
        if(speed != 0)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
        else
        {
            Debug.Log("No Speed");
        }
    }

    void OnCollisionEnter(Collision other)
    {
        speed = 0;

        ContactPoint contact = other.contacts[0];
        Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
        Vector3 pos = contact.point;

        photonView.RPC("SpawnHit", RpcTarget.All, rot, pos);
    }

    [PunRPC]
    public void CastSpell()
    {
        if(muzzlePrefab != null)
        {
            var muzzleVFX = Instantiate(muzzlePrefab, transform.position, Quaternion.identity);
            muzzleVFX.transform.forward = gameObject.transform.forward;

            var psMuzzle = muzzleVFX.GetComponent<ParticleSystem>();

            if(psMuzzle != null)
            {
                Destroy(muzzleVFX, timeToDestroy);
            }
            else
            {
                Destroy(muzzleVFX, timeToDestroy);
            }
        }
    }

    [PunRPC]
    public void SpawnHit(Quaternion rot, Vector3 pos)
    {
        if(hitPrefab != null)
        {
            var hitVFX = Instantiate(hitPrefab, pos, rot);

            var psHit = hitVFX.GetComponent<ParticleSystem>();

            if(psHit != null)
            {
                Destroy(hitVFX, timeToDestroy);
            }
            else
            {
                Destroy(hitVFX, timeToDestroy);
            }
        }

        Destroy(gameObject);
    }
}
