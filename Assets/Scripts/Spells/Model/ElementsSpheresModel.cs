﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementsSpheresModel : MonoBehaviourPunCallbacks
{
    [Header("Materials")]
    public Material[] materials;

    [Header("Spheres")]
    public GameObject[] spheres;

    ElementsModel elementsModel;
    
    void Start()
    {
        elementsModel = FindObjectOfType<ElementsModel>();
    }

    public void CallRPC(int index, Element element)
    {
        photonView.RPC("UpdateColor", RpcTarget.All, index, element);
    }

    [PunRPC]
    void UpdateColor(int index, Element element)
    {
        string[] elementsEnumArr = System.Enum.GetNames(typeof(Element)); ;

        if (!spheres[index].activeSelf)
        {
            spheres[index].SetActive(true);
        }
        
        for (int i = 1; i < elementsEnumArr.Length; i++)
        {
            if (element.ToString() == elementsEnumArr[i])
            {
                spheres[index].GetComponent<MeshRenderer>().material = materials[i - 1];
            }
        }
    }
}
