﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Element
{
    None,
    Fire,
    Water,
    Earth,
    Air,
    Light,
    Dark,
}

public class ElementsModel : MonoBehaviour
{
    [Header("Variables")]
    public int maxElements = 6;
    public int currentElements = 0;
    public int timeBeforeAddElement = 10;

    [Space]
    [Header("Elements")]
    public Element[] elementsCombination = new Element[2];
    public Element[] elementsQueue = new Element[Enum.GetValues(typeof(Element)).Length - 1];
}
