﻿using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SetWaitingLabelCommand : EventCommand
{

    public override void Execute()
    {
        Debug.Log("Set waiting label");
        dispatcher.Dispatch(LauncherEvents.SetWaitingLabel);
    }
}
