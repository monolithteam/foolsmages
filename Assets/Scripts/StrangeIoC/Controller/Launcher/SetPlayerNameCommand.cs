﻿using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class SetPlayerNameCommand : EventCommand
{
    [Inject]
    public ILauncher launcher { get; set; }

    public override void Execute()
    {
        string name = evt.data as string;

        launcher.SetName(name);
    }
}
