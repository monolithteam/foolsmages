﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;
using Photon.Realtime;
using Photon.Pun;

public class CreateRoomCommand : EventCommand
{
    [Inject]
    public ILauncherModel model { get; set; }

    [Inject]
    public ILauncher launcher { get; set; }

    public override void Execute()
    {
        Debug.Log("Creating new room");

        launcher.CreateNewRoom(model.maxPlayersPerRoom);
    }
}
