﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSearchingCommand : EventCommand
{
    [Inject]
    public ILauncher launcher { get; set; }

    [Inject]
    public ILauncherModel model { get; set; }

    public override void Execute()
    {
        Debug.Log("Start searching");

        model.gameVersion = "1";
        model.maxPlayersPerRoom = 4;
        model.isConnecting = false;

        launcher.Connect(model.gameVersion);
    }
}
