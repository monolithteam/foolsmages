﻿using strange.extensions.command.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class UpdatePlayersCountCommand : EventCommand
{
    [Inject]
    public ILauncherModel model { get; set; }

    public override void Execute()
    {
        Debug.Log("Update players counter");

        byte playersCount = (byte)evt.data;

        byte[] parameters = new byte[2];
        parameters[0] = playersCount;
        parameters[1] = model.maxPlayersPerRoom;

        dispatcher.Dispatch(LauncherEvents.UpdatePlayersCounter, parameters);
    }
}
