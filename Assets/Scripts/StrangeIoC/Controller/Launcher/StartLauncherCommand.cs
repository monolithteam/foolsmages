﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.api;
using strange.extensions.command.impl;
using strange.extensions.dispatcher.eventdispatcher.impl;

public class StartLauncherCommand : EventCommand
{
    public override void Execute()
    {
        GameObject _launcherUI = Resources.Load("UI/LauncherUI") as GameObject;

        if (_launcherUI == null)
        {
            Debug.Log("LaucherUI in Resources/UI is missing!");
            return;
        }

        GameObject.Instantiate(_launcherUI);
        Debug.Log("LauncherUI loaded!");
    }
}
