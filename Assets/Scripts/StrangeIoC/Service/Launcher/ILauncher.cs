﻿using strange.extensions.dispatcher.eventdispatcher.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public interface ILauncher
{
    IEventDispatcher dispatcher { get; set; }

    void Connect(string gameVersion);
    void SetName(string name);
    void CreateNewRoom(byte player);
}
