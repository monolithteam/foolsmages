﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.context.api;

public class Launcher : MonoBehaviourPunCallbacks, ILauncher
{

    [Inject(ContextKeys.CONTEXT_DISPATCHER)]
    public IEventDispatcher dispatcher { get; set; }

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    public void Connect(string gameVersion)
    {
        Debug.Log("Connect was called");
        
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinRandomRoom();
        }
        else
        {
            PhotonNetwork.GameVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public void SetName(string name)
    {
        Debug.Log("Set nickname: " + name);
        PhotonNetwork.NickName = name;
    }

    public void CreateNewRoom(byte player)
    {
        PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = player });
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster() was called by PUN");

        Debug.Log("JoinRandomRoom() was called");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogWarningFormat("OnDisconnected() was called by PUN with reason {0}", cause);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed() was called by Pun");
        
        dispatcher.Dispatch(LauncherEvents.StartCreateNewRoom);
    }

    public override void OnJoinedRoom()
    {
        dispatcher.Dispatch(LauncherEvents.CallWaitingLabel);
        photonView.RPC("RPCUpdatePlayersCount", RpcTarget.All);
        
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            photonView.RPC("RPCLoadNextScene", RpcTarget.All);
        }
    }

    [PunRPC]
    public void RPCUpdatePlayersCount()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
        dispatcher.Dispatch(LauncherEvents.CallPlayersCounter, PhotonNetwork.CurrentRoom.PlayerCount);
    }

    [PunRPC]
    public void RPCLoadNextScene()
    {
        PhotonNetwork.LoadLevel(1);
    }
}
