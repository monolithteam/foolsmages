﻿public interface ILauncherModel
{
    byte maxPlayersPerRoom { get; set; }
    string gameVersion { get; set; }
    bool isConnecting { get; set; }
}
