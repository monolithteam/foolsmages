﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherModel : ILauncherModel
{
    public byte maxPlayersPerRoom { get; set; }
    public string gameVersion { get; set; }
    public bool isConnecting { get; set; }
}
