﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerMediator : EventMediator
{
    [Inject]
    public GameManagerView view { get; set; }

    public override void OnRegister()
    {
        view.Init();
    }

    public override void OnRemove()
    {
        
    }
}
