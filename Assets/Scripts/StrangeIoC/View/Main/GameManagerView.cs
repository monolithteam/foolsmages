﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerView : BaseView
{
    [Header("Spawn Points")]
    [SerializeField]
    private List<Transform> spawnPoints;

    [Header("Player Components")]
    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private Transform playerParent;

    PhotonView photonView;

    internal void Init()
    {
        photonView = GetComponent<PhotonView>();

        Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);
        
        int r = PhotonNetwork.LocalPlayer.ActorNumber - 1;

        GameObject _player = PhotonNetwork.Instantiate(this.playerPrefab.name, spawnPoints[r].position, Quaternion.identity, 0);
        _player.name = PhotonNetwork.LocalPlayer.NickName;
        _player.transform.SetParent(playerParent);
        photonView.RPC("RPCUpdateSpawnPoints", RpcTarget.All, r);
    }

    [PunRPC]
    public void RPCUpdateSpawnPoints(int r)
    {
        spawnPoints.RemoveAt(r);
    }
}
