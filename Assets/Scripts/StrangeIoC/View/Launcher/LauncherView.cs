﻿using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LauncherView : BaseView
{
    [Header("GameObjects")]
    [SerializeField]
    private GameObject controlPanel;
    [SerializeField]
    private GameObject progressLabel;
    [SerializeField]
    private GameObject waitingLabel;

    [Header("Buttons")]
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Button testVFXButton;

    [Header("Inputs")]
    [SerializeField]
    private InputField nameInput;

    [Header("Texts")]
    [SerializeField]
    private Text playerCount;
    
    private const string playerNamePrefKey = "PlayerName";

    internal void Init()
    {
        string defaultName = string.Empty;

        if (nameInput != null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefKey))
            {
                defaultName = PlayerPrefs.GetString(playerNamePrefKey);
                nameInput.text = defaultName;
            }
        }
        
        playButton.onClick.AddListener(OnStartSearching);
        testVFXButton.onClick.AddListener(OnStartTestVFX);
        nameInput.onValueChanged.AddListener(SetPlayerName);

        dispatcher.Dispatch(LauncherEvents.SetPlayerName, defaultName);

        SetWindow(true);
    }

    internal void SetWaitingLabel()
    {
        controlPanel.SetActive(false);
        progressLabel.SetActive(false);
        waitingLabel.SetActive(true);
    }

    internal void SetPlayersCounter(IEvent evt)
    {
        byte[] playersCount = (byte[])evt.data;

        playerCount.text = playersCount[0].ToString() + "/" + playersCount[1].ToString();
    }

    void OnStartSearching()
    {
        Debug.Log("OnStartSearching clicked");
        SetWindow(false);
        dispatcher.Dispatch(LauncherEvents.StartSearching);
    }

    void OnStartTestVFX()
    {
        Debug.Log("OnStartTestVFX clicked");
        dispatcher.Dispatch(LauncherEvents.TestVFXScene);
    }

    void SetWindow(bool active)
    {
        controlPanel.SetActive(active);
        progressLabel.SetActive(!active);
        waitingLabel.SetActive(false);
    }
    
    void SetPlayerName(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            Debug.Log("Player Name is null or empty");
            return;
        }

        dispatcher.Dispatch(LauncherEvents.SetPlayerName, name);

        PlayerPrefs.SetString(playerNamePrefKey, name);
    }
}
