﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherMediator : EventMediator
{
    [Inject]
    public LauncherView view { get; set; }

    public override void OnRegister()
    {
        dispatcher.AddListener(LauncherEvents.UpdatePlayersCounter, view.SetPlayersCounter);
        dispatcher.AddListener(LauncherEvents.SetWaitingLabel, view.SetWaitingLabel);

        view.Init();
    }

    public override void OnRemove()
    {
        dispatcher.RemoveListener(LauncherEvents.UpdatePlayersCounter, view.SetPlayersCounter);
        dispatcher.RemoveListener(LauncherEvents.SetWaitingLabel, view.SetWaitingLabel);
    }
}
