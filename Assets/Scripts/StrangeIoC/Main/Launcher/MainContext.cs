﻿using strange.extensions.context.api;
using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainContext : MVCSContext
{
    public MainContext(MonoBehaviour view) : base(view)
    {

    }

    public MainContext(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {

    }

    public override void Launch()
    {
        dispatcher.Dispatch(LauncherEvents.StartLauncher);
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
    }

    protected override void mapBindings()
    {
        //Injections
        injectionBinder.Bind<ILauncherModel>().To<LauncherModel>().ToSingleton();

        //Mediators
        mediationBinder.Bind<LauncherView>().To<LauncherMediator>();

        //Commands
        commandBinder.Bind(LauncherEvents.StartLauncher).To<StartLauncherCommand>();
        commandBinder.Bind(LauncherEvents.StartSearching).To<StartSearchingCommand>();
        commandBinder.Bind(LauncherEvents.StartCreateNewRoom).To<CreateRoomCommand>();
        commandBinder.Bind(LauncherEvents.SetPlayerName).To<SetPlayerNameCommand>();
        commandBinder.Bind(LauncherEvents.CallPlayersCounter).To<UpdatePlayersCountCommand>();
        commandBinder.Bind(LauncherEvents.CallWaitingLabel).To<SetWaitingLabelCommand>();
        commandBinder.Bind(LauncherEvents.TestVFXScene).To<TestVFXCommand>();

        //Services
        Launcher launcher = GameObject.FindObjectOfType<Launcher>().GetComponent<Launcher>();
        injectionBinder.Bind<ILauncher>().ToValue(launcher);
    }
}
