﻿public enum LauncherEvents
{
    //Launcher
    StartLauncher,
    StartSearching,
    StartCreateNewRoom,
    SetPlayerName,
    SetWaitingLabel,
    CallPlayersCounter,
    CallWaitingLabel,
    UpdatePlayersCounter,
    TestVFXScene,
}