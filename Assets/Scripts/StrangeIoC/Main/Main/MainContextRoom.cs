﻿using strange.extensions.context.api;
using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainContextRoom : MVCSContext
{
    public MainContextRoom(MonoBehaviour view) : base(view)
    {

    }

    public MainContextRoom(MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
    {

    }

    public override void Launch()
    {

    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
    }

    protected override void mapBindings()
    {
        //Injections
        ElementsModel elementsModel = GameObject.FindObjectOfType<ElementsModel>().GetComponent<ElementsModel>();
        injectionBinder.Bind<ElementsModel>().ToValue(elementsModel);
        injectionBinder.Bind<SpellModel>().ToSingleton();

        //Mediators
        mediationBinder.Bind<GameManagerView>().To<GameManagerMediator>();
        mediationBinder.Bind<SpellView>().To<SpellMediator>();

        //Commands
        commandBinder.Bind(MainEvents.CastSpell).To<CastSpellController>();
        commandBinder.Bind(MainEvents.SpellDatabase).To<SpellDatabaseController>();
        //Services
    }
}
