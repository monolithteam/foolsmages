﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainContextViewRoom : ContextView
{
    void Awake()
    {
        context = new MainContextRoom(this);
        context.Launch();
    }
}
