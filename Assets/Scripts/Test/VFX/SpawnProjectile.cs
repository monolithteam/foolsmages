﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProjectile : MonoBehaviour
{
    public GameObject vfx;

    private GameObject firePoint;
    private GameObject effectToSpawn;
    private float timeToFire = 0;

    void Start()
    {
        effectToSpawn = vfx;
        firePoint = gameObject;
    }

    void Update()
    {
        if(Input.GetMouseButtonUp(0) && Time.time >= timeToFire)
        {
            timeToFire = Time.time + 1 / 1f;

            SpawnVFX();
        }
    }

    void SpawnVFX()
    {
        GameObject vfx;

        if(firePoint != null)
        {
            vfx = Instantiate(effectToSpawn, firePoint.transform.position, Quaternion.identity);

            vfx.transform.localRotation = firePoint.transform.rotation;
        }
        else
        {
            Debug.Log("No Fire Point");
        }
    }
}
