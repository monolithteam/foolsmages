﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private List<Transform> spawnPoints;

    [SerializeField]
    private GameObject playerPrefab;

    [SerializeField]
    private Transform parentPlayers;

    public static GameManager Instance;

    void Start()
    {
        Instance = this;

        InitializeManager();
    }

    void InitializeManager()
    {
        if (PlayerManager.LocalPlayerInstance == null)
        {
            Debug.LogFormat("We are Instantiating LocalPlayer from {0}", SceneManagerHelper.ActiveSceneName);

            int r = Random.Range(0, spawnPoints.Count);

            GameObject _player = PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[r].position, Quaternion.identity, 0);
            _player.name = PhotonNetwork.LocalPlayer.NickName;
            spawnPoints.RemoveAt(r);
        }
        else
        {
            Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
        }
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene(0);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
}
