﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
{
    public int maxHealth = 100;
    [HideInInspector]
    public int currentHealth;
    
    public GameObject playerUIPrefab;

    public static GameObject LocalPlayerInstance;
    
    void Awake()
    {
        if (photonView.IsMine)
        {
            PlayerManager.LocalPlayerInstance = this.gameObject;
        }
    }

    void Start()
    {
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;

        currentHealth = maxHealth;

        if (playerUIPrefab != null)
        {
            GameObject _uiGo = Instantiate(playerUIPrefab);
            _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
        }
        else
        {
            Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
        }
    }

    void Update()
    {
        if (currentHealth <= 0)
        {
            PhotonNetwork.LeaveRoom();
            SceneManager.LoadScene(0);
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(currentHealth);
        }
        else
        {
            // Network player, receive data
            this.currentHealth = (int)stream.ReceiveNext();
        }
    }

    void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode loadingMode)
    {
        this.CalledOnLevelWasLoaded(scene.buildIndex);
    }

    void OnLevelWasLoaded(int level)
    {
        this.CalledOnLevelWasLoaded(level);
    }

    void CalledOnLevelWasLoaded(int level)
    {
        GameObject _uiGo = Instantiate(this.playerUIPrefab);
        _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
    }

    public override void OnDisable()
    {
        // Always call the base to remove callbacks
        base.OnDisable();
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    public void DecreaseHealth(int value)
    {
        Debug.Log("Get damage: <color=#FF5733>" + value + "</color>");
        currentHealth -= value;
    }

    public void IncreaseHealth(int value)
    {
        Debug.Log("Get heal: <color=#33FF6E>" + value + "</color>");
        currentHealth += value;
    }
}
